package com.luv2code.sp.service;

import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;

public interface SignUpService {
	public boolean saveForUser(UserInfo userInfo);
	public boolean saveForAdmin(AdminInfo adminInfo);
}
