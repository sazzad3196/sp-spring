package com.luv2code.sp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="iteminfo")
public class ShoppingListInfo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	int id;
	
	@Column(name="name")
	String name;
	
	@Column(name="catagory")
	String catagory;
	
	@Column(name="image")
	String image;
	
	@Column(name="price")
	int price;
	
	@OneToMany(mappedBy="shoppingListInfo", fetch =FetchType.LAZY,  cascade= {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH})
	List<CardInfo> cardInfos;
	 
	
	public ShoppingListInfo() {}
	
	public ShoppingListInfo(String name, String catagory, String image, int price) {
		this.name = name;
		this.catagory = catagory;
		this.image = image;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCatagory() {
		return catagory;
	}

	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public List<CardInfo> getCardInfos() {
		return cardInfos;
	}

	public void setCardInfos(List<CardInfo> cardInfos) {
		this.cardInfos = cardInfos;
	}

	@Override
	public String toString() {
		return "ShoppingListInfo [id=" + id + ", name=" + name + ", catagory=" + catagory + ", image=" + image
				+ ", price=" + price + "]";
	}
	
	public void add(CardInfo cardInfo) {
		if(cardInfos == null) {
			cardInfos = new ArrayList<>();
		}
		
		cardInfos.add(cardInfo);
		cardInfo.setShoppingListInfo(this);
	}
	
	
}
