package com.luv2code.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.sp.dao.SignInDao;
import com.luv2code.sp.entity.UserInfo;
import com.luv2code.sp.service.SignInService;


@RestController
@RequestMapping("/login")
public class LoginController {
	@Autowired
	private SignInService signInService;
	
	@RequestMapping("/user")
	public UserInfo checkUserInfo(@RequestParam String email, @RequestParam String password) {
		return signInService.checkUserValidOrNot(email, password);
	}
	
	@RequestMapping("/admin")
	public boolean checkAdminInfo(@RequestParam String email, @RequestParam String password) {
		return signInService.checkAdminValidOrNot(email, password);
	}
}
