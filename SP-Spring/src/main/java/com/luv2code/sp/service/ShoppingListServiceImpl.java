package com.luv2code.sp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.sp.dao.ShoppingListDao;
import com.luv2code.sp.entity.ShoppingListInfo;

@Service
public class ShoppingListServiceImpl implements ShoppingListService {

	@Autowired
	ShoppingListDao shoppingListDao;
	
	@Transactional
	@Override
	public boolean saveOrUpdateShoppingList(ShoppingListInfo shoppingListInfo) {
		return shoppingListDao.saveOrUpdateShoppingList(shoppingListInfo);
	}

	@Transactional
	@Override
	public List<ShoppingListInfo> findAllShoppingList() {
		return shoppingListDao.findAllShoppingList();
	}

	@Transactional
	@Override
	public boolean deleteShoppingList(int id) {
		return shoppingListDao.deleteShoppingList(id);
	}

	@Transactional
	@Override
	public List<ShoppingListInfo> getBreadFromShoppingList() {
		return shoppingListDao.getBreadFromShoppingList();
	}

	@Transactional
	@Override
	public List<ShoppingListInfo> getVegetablesFromShoppingList() {
		return shoppingListDao.getVegetablesFromShoppingList();
	}

	@Transactional
	@Override
	public List<ShoppingListInfo> getFruitsFromShoppingList() {
		return shoppingListDao.getFruitsFromShoppingList();
	}

	@Transactional
	@Override
	public List<ShoppingListInfo> getDairyFromShoppingList() {
		return shoppingListDao.getDairyFromShoppingList();
	}

}
