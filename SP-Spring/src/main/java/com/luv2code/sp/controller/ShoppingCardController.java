package com.luv2code.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.sp.entity.CardInfo;
import com.luv2code.sp.service.ShoppingCardService;

@RestController
@RequestMapping("/shoppingCard")
public class ShoppingCardController {
	@Autowired
	private ShoppingCardService shoppingCardService;
	
	@GetMapping("/saveOrUpdate")
	public boolean saveOrUpdateShoppingCard(@RequestParam("userId") int userId, @RequestParam("itemId") int itemId, @RequestParam("quantity") int quantity) {
		System.out.println("Quantity: " + quantity);
		return shoppingCardService.saveOrUpdateShoppingCard(userId, itemId, quantity);
	}
	
	@PostMapping("/saveOrUpdate")
	public boolean saveOrUpdateShoppingCard(@RequestBody CardInfo theCardInfo) {
		return shoppingCardService.saveOrUpdateShoppingCard(theCardInfo);
	}
	
	@GetMapping("/getCardInfoByUserId")
	public List<CardInfo> getCardInfoByUserId(@RequestParam("userId") int userId, @RequestParam("status") int status) {
		  return shoppingCardService.getCardInfoByUserId(userId, status);
	}
	
	@GetMapping("/delete")
	public boolean deleteShoppingCard(@RequestParam("id") int id) {
		return shoppingCardService.deleteShoppingCard(id);
	}
	
	@GetMapping("/getCardInfoByCardId")
	public CardInfo getCardInfoByCardId(@RequestParam("cardId") int cardId) {
		  return shoppingCardService.getCardInfoByCardId(cardId);
	}
}
