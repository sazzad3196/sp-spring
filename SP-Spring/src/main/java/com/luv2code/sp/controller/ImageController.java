package com.luv2code.sp.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class ImageController {
	//private static String UPLOADED_FOLDER = "\\META-INF.resources\\images\\";
	
	@RequestMapping(value="/uploadImage", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String uploadImage(@RequestPart("file") MultipartFile file) {
		String img = System.currentTimeMillis() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
		try {   
			String path = "E:\\Image\\" + img;
			File f = new File(path);
			InputStream is = file.getInputStream();
			FileOutputStream out = new FileOutputStream(path);
			byte buf[] = new byte[1024];
	        int len;
	        while ((len = is.read(buf)) > 0)
	            out.write(buf, 0, len);
	        is.close();
	        out.close();
		    return img;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	
	@RequestMapping(value="/getImage", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public @ResponseBody byte[] getImage(@RequestParam("image") String image) throws IOException {
		String path = "E:\\Image\\" + image;
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
        int len;
        while ((len = fis.read(buf)) > 0) {
            bos.write(buf, 0, len);
        }
        fis.close();
        return bos.toByteArray();
	}
	
	
}
