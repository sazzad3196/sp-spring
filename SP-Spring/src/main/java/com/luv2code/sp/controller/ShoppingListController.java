package com.luv2code.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.sp.entity.ShoppingListInfo;
import com.luv2code.sp.service.ShoppingListService;

@RestController
@RequestMapping("/shoppingList")
public class ShoppingListController {
	@Autowired
	ShoppingListService shoppingListService;
	
	
	@PostMapping("/saveOrUpdate")
	public boolean saveOrUpdateShoppingList(@RequestBody ShoppingListInfo shoppingListInfo) {
		return shoppingListService.saveOrUpdateShoppingList(shoppingListInfo);
	}
	
	@GetMapping("/findAll")
	public List<ShoppingListInfo> findAllShoppingList() {
		 return shoppingListService.findAllShoppingList();
	}
	
	@GetMapping("delete/{id}")
	public boolean deleteShoppingList(@PathVariable("id") int id) {
		return shoppingListService.deleteShoppingList(id);
	}
	
	@GetMapping("/getBread")
	public List<ShoppingListInfo> getBreadFromShoppingList() {
		 List<ShoppingListInfo> list = shoppingListService.getBreadFromShoppingList(); 
		 return list;
	}
	
	@GetMapping("/getVegetables")
	public List<ShoppingListInfo> getVegetablesFromShoppingList() {
		 return shoppingListService.getVegetablesFromShoppingList();
	}
	
	@GetMapping("/getDairy")
	public List<ShoppingListInfo> getDairyFromShoppingList() {
		 return shoppingListService.getDairyFromShoppingList();
	}
	
	@GetMapping("/getFruits")
	public List<ShoppingListInfo> getFruitsFromShoppingList() {
		 return shoppingListService.getFruitsFromShoppingList();
	}
	
	
}
