package com.luv2code.sp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.sp.dao.ShoppingCardDao;
import com.luv2code.sp.entity.CardInfo;

@Service
public class ShoppingCardServiceImpl implements ShoppingCardService {

	@Autowired
	private ShoppingCardDao shoppingCardDao;
	
	@Transactional
	@Override
	public boolean saveOrUpdateShoppingCard(int userId, int itemId, int quantity) {
		return shoppingCardDao.saveOrUpdateShoppingCard(userId, itemId, quantity);
	}

	@Transactional
	@Override
	public List<CardInfo> getCardInfoByUserId(int userId, int status) {
		return shoppingCardDao.getCardInfoByUserId(userId, status);
	}

	@Transactional
	@Override
	public boolean deleteShoppingCard(int id) {
		return shoppingCardDao.deleteShoppingCard(id);
	}

	@Transactional
	@Override
	public CardInfo getCardInfoByCardId(int cardId) {
		return shoppingCardDao.getCardInfoByCardId(cardId);
	}

	@Override
	public boolean saveOrUpdateShoppingCard(CardInfo cardInfo) {
		return shoppingCardDao.saveOrUpdateShoppingCard(cardInfo);
	}

}
