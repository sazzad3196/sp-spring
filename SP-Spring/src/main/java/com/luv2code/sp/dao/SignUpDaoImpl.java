package com.luv2code.sp.dao;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;

@Repository
@Transactional
public class SignUpDaoImpl implements SignUpDao {

	@Autowired
	EntityManager entityManager;
	
	@Override
	public boolean saveForUser(UserInfo userInfo) {
		entityManager.persist(userInfo);
		return true;
	}

	@Override
	public boolean saveForAdmin(AdminInfo adminInfo) {
		entityManager.persist(adminInfo);
		return true;
	}

}
