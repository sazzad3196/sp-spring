package com.luv2code.sp.dao;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.luv2code.sp.entity.OrderInfo;

public interface ShoppingOrderDao {
	 public boolean saveOrUpdateShoppingOrder(int cardId, int userId, String address, String date, String city);
	 public List<OrderInfo> findAllByUserIdShoppingOrder(int userId);
	 public List<OrderInfo> findAllShoppingOrder();
	 public boolean deleteShoppingOrder(int orderId);
	 public OrderInfo getOrderInfoByOrderId(int orderId);
}
