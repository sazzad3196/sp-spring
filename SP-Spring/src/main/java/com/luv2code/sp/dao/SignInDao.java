package com.luv2code.sp.dao;

import com.luv2code.sp.entity.UserInfo;

public interface SignInDao {
	public UserInfo checkUserValidOrNot(String email, String password);
	public boolean checkAdminValidOrNot(String email, String password);
}
