package com.luv2code.sp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="orderinfo")
public class OrderInfo {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="address")
	private String address;
	
	@Column(name="date")
	private String date;
	
	@Column(name="city")
	private String city;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH})
	@JsonIgnoreProperties("orderInfos")
	@JoinColumn(name="userId")
	private UserInfo userInfo;
	 
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH})
	@JsonIgnoreProperties("orderInfos")
	@JoinColumn(name="cardId")
	private CardInfo cardInfo;
	
	public OrderInfo() {}
	
	public OrderInfo(String address, String date, String city) {
		super();
		this.address = address;
		this.date = date;
		this.city = city;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public CardInfo getCardInfo() {
		return cardInfo;
	}

	public void setCardInfo(CardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}

	@Override
	public String toString() {
		return "OrderInfo [id=" + id + ", address=" + address + ", date=" + date + ", userInfo=" + userInfo
				+ ", cardInfo=" + cardInfo + "]";
	}
	
	
}
