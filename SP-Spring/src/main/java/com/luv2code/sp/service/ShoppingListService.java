package com.luv2code.sp.service;

import java.util.List;

import com.luv2code.sp.entity.ShoppingListInfo;

public interface ShoppingListService {
	 public boolean deleteShoppingList(int id);
	 public boolean saveOrUpdateShoppingList(ShoppingListInfo shoppingListInfo);
	 public List<ShoppingListInfo> findAllShoppingList();
	 public List<ShoppingListInfo> getBreadFromShoppingList();
	 public List<ShoppingListInfo> getVegetablesFromShoppingList();
	 public List<ShoppingListInfo> getFruitsFromShoppingList();
	 public List<ShoppingListInfo> getDairyFromShoppingList();
}
