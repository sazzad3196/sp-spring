package com.luv2code.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;
import com.luv2code.sp.service.SignUpService;

@RestController
@RequestMapping("/register")
public class RegisterController {
	@Autowired
	SignUpService theSignUpService;
	
	@RequestMapping(value = "/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean signUpUser(@RequestBody UserInfo userInfo) {
		if(userInfo != null) {
			System.out.println("Name: " + userInfo.getName());
			System.out.println("Email: " + userInfo.getEmail());
		}
		return theSignUpService.saveForUser(userInfo);
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean signUpAdmin(@RequestBody AdminInfo adminInfo) {
		return theSignUpService.saveForAdmin(adminInfo);
	}
}
