package com.luv2code.sp.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.sp.dao.SignUpDao;
import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;

@Service
public class SignUpServiceImpl implements SignUpService {

	@Autowired
	SignUpDao theSignUpDao;
	
	@Transactional
	@Override
	public boolean saveForUser(UserInfo userInfo) {
		return theSignUpDao.saveForUser(userInfo);
	}

	@Transactional
	@Override
	public boolean saveForAdmin(AdminInfo adminInfo) {
		return theSignUpDao.saveForAdmin(adminInfo);
	}

}
