package com.luv2code.sp.dao;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.luv2code.sp.entity.CardInfo;

public interface ShoppingCardDao {
	 public boolean saveOrUpdateShoppingCard(CardInfo cardInfo);
	 public boolean saveOrUpdateShoppingCard(int userId, int itemId, int quantity);
	 public List<CardInfo> getCardInfoByUserId(int userId, int status);
	 public boolean deleteShoppingCard(int id);
	 public CardInfo getCardInfoByCardId(int cardId);
}
