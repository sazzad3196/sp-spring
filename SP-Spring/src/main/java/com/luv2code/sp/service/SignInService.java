package com.luv2code.sp.service;

import com.luv2code.sp.entity.UserInfo;

public interface SignInService {
	public UserInfo checkUserValidOrNot(String email, String password);
	public boolean checkAdminValidOrNot(String email, String password);
}
