package com.luv2code.sp.dao;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;

@Repository
@Transactional
public class SignInDaoImpl implements SignInDao {

	@Autowired
	private EntityManager entityManager;
	
	@Override
	public UserInfo checkUserValidOrNot(String email, String password) {
		Query theQuery = entityManager.createQuery("from UserInfo where email='" + email + "' and password='" + password + "'", UserInfo.class);
		UserInfo theUserInfo = (UserInfo) theQuery.getSingleResult();
		System.out.println("User Info: " + theUserInfo);
		if(theUserInfo != null) {
			return theUserInfo;
		}
		return null;
	}

	@Override
	public boolean checkAdminValidOrNot(String email, String password) {
		Query theQuery = entityManager.createQuery("from AdminInfo where email='" + email + "' and password='" + password + "'", AdminInfo.class);
		AdminInfo theAdminInfo = (AdminInfo) theQuery.getSingleResult();
		System.out.println("Admin Info: " + theAdminInfo);
		if(theAdminInfo != null) {
			return true;
		}
		return false;
	}

}
