package com.luv2code.sp.service;

import java.util.List;

import com.luv2code.sp.entity.CardInfo;

public interface ShoppingCardService {
	 public boolean saveOrUpdateShoppingCard(CardInfo cardInfo);
	 public boolean saveOrUpdateShoppingCard(int userId, int itemId, int quantity);
	 public List<CardInfo> getCardInfoByUserId(int userId, int status);
	 public boolean deleteShoppingCard(int id);
	 public CardInfo getCardInfoByCardId(int cardId); 
}
