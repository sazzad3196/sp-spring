package com.luv2code.sp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.sp.dao.ShoppingOrderDao;
import com.luv2code.sp.entity.OrderInfo;

@Service
public class ShoppingOrderServiceImpl implements ShoppingOrderService {
	@Autowired
	private ShoppingOrderDao theShoppingOrderDao;
	
	@Transactional
	@Override
	public boolean saveOrUpdateShoppingOrder(int cardId, int userId, String address, String date, String city) {
		return theShoppingOrderDao.saveOrUpdateShoppingOrder(cardId, userId, address, date, city);
	}

	@Transactional
	public List<OrderInfo> findAllByUserIdShoppingOrder(int userId) {
		return theShoppingOrderDao.findAllByUserIdShoppingOrder(userId);
	}

	@Override
	@Transactional
	public List<OrderInfo> findAllShoppingOrder() {
		return theShoppingOrderDao.findAllShoppingOrder();
	}

	@Override
	@Transactional
	public boolean deleteShoppingOrder(int orderId) {
		return theShoppingOrderDao.deleteShoppingOrder(orderId);
	}

	@Override
	@Transactional
	public OrderInfo getOrderInfoByOrderId(int orderId) {
		return theShoppingOrderDao.getOrderInfoByOrderId(orderId);
	}

}
