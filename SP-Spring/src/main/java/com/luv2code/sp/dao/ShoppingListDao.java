package com.luv2code.sp.dao;

import java.util.List;

import com.luv2code.sp.entity.ShoppingListInfo;

public interface ShoppingListDao {
	 public boolean saveOrUpdateShoppingList(ShoppingListInfo shoppingListInfo);
	 public List<ShoppingListInfo> findAllShoppingList();
	 public boolean deleteShoppingList(int id);
	 public List<ShoppingListInfo> getBreadFromShoppingList();
	 public List<ShoppingListInfo> getVegetablesFromShoppingList();
	 public List<ShoppingListInfo> getFruitsFromShoppingList();
	 public List<ShoppingListInfo> getDairyFromShoppingList();
}
