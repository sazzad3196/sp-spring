package com.luv2code.sp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="cardinfo")
public class CardInfo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="userId")
	private int userId;

	@Column(name="quantity")
	private int quantity;
	
	@Column(name="status")
	private int status;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="itemId")
	@JsonIgnoreProperties("cardInfos")
	private ShoppingListInfo shoppingListInfo;
	
	@OneToMany(mappedBy="cardInfo", cascade= {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH})
	private List<OrderInfo> orderInfos;
	
	public CardInfo() {
		
	}

	public CardInfo(int userId, int quantity, int status) {
		super();
		this.userId = userId;
		this.quantity = quantity;
		this.status = status;
	}
	
	public void add(OrderInfo theOrderInfo) {
		if(orderInfos == null) {
			orderInfos = new ArrayList();
		}
		
		orderInfos.add(theOrderInfo);
		theOrderInfo.setCardInfo(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ShoppingListInfo getShoppingListInfo() {
		return shoppingListInfo;
	}

	public void setShoppingListInfo(ShoppingListInfo shoppingListInfo) {
		this.shoppingListInfo = shoppingListInfo;
	}
	
	public List<OrderInfo> getOrderInfos() {
		return orderInfos;
	}

	public void setOrderInfos(List<OrderInfo> orderInfos) {
		this.orderInfos = orderInfos;
	}

	@Override
	public String toString() {
		return "CardInfo [id=" + id + ", userId=" + userId + ", quantity=" + quantity + ", status=" + status
				+ ", shoppingListInfo=" + shoppingListInfo + ", orderInfos=" + orderInfos + "]";
	}

	
	
}
