package com.luv2code.sp.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.sp.entity.OrderInfo;
import com.luv2code.sp.service.ShoppingOrderService;

@RestController
@RequestMapping("/shoppingOrder")
public class ShoppingOrderController {
	 @Autowired
	 private ShoppingOrderService theShoppingOrderService;
	 
	 @GetMapping("/saveOrUpdate")
	 public boolean saveOrUpdateShoppingOrder(@RequestParam("cardId") int cardId, @RequestParam("userId") int userId, @RequestParam("address") String address
			     , @RequestParam("date") String date, @RequestParam("city") String city) {
		 //System.out.println("Address: " + address);
		 return theShoppingOrderService.saveOrUpdateShoppingOrder(cardId, userId, address, date, city);
	 }
	 
	 @GetMapping("/findAllByUserId")
	 public List<OrderInfo> findAllByUserIdShoppingOrder(@RequestParam("userId") int userId) {
		 return theShoppingOrderService.findAllByUserIdShoppingOrder(userId);
	 }
	 
	 @GetMapping("/findAll")
	 public List<OrderInfo> findAllShoppingOrder() {
		 return theShoppingOrderService.findAllShoppingOrder();
	 }
	 
	 @GetMapping("/delete")
	 public boolean deleteShoppingOrder(@RequestParam("orderId") int orderId) {
		 return theShoppingOrderService.deleteShoppingOrder(orderId);
	 }
	 
	 @GetMapping("/getOrderInfoByOrderId")
	 public OrderInfo getOrderInfoByOrderId(@RequestParam("orderId") int orderId) {
		 return theShoppingOrderService.getOrderInfoByOrderId(orderId);
	 }
	 
}
