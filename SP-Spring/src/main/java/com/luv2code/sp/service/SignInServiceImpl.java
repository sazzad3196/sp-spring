package com.luv2code.sp.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.sp.dao.SignInDao;
import com.luv2code.sp.entity.UserInfo;

@Service
public class SignInServiceImpl implements SignInService {

	private SignInDao signInDao;
	
	@Autowired
	public SignInServiceImpl(SignInDao signInDao) {
		this.signInDao = signInDao;
	}
	
	@Override
	@Transactional
	public UserInfo checkUserValidOrNot(String email, String password) {
		return signInDao.checkUserValidOrNot(email, password);
	}

	@Override
	@Transactional
	public boolean checkAdminValidOrNot(String email, String password) {
		return signInDao.checkAdminValidOrNot(email, password);
	}

}
