package com.luv2code.sp.dao;

import com.luv2code.sp.entity.AdminInfo;
import com.luv2code.sp.entity.UserInfo;

public interface SignUpDao {
	public boolean saveForUser(UserInfo userInfo);
	public boolean saveForAdmin(AdminInfo adminInfo);
}
