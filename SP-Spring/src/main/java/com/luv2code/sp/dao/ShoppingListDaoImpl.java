package com.luv2code.sp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.stereotype.Repository;

import com.luv2code.sp.entity.ShoppingListInfo;

@Repository
@Transactional
public class ShoppingListDaoImpl implements ShoppingListDao {

	@Autowired
	private EntityManager entityManager;
	
	@Override
	public boolean saveOrUpdateShoppingList(ShoppingListInfo shoppingListInfo) {
		try {
			entityManager.merge(shoppingListInfo);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<ShoppingListInfo> findAllShoppingList() {
		Query theQuery = entityManager.createQuery("from ShoppingListInfo");
		List<ShoppingListInfo> theList = theQuery.getResultList();
		return theList;
	}

	@Override
	public boolean deleteShoppingList(int id) {
		try {
			ShoppingListInfo shoppingListInfo = entityManager.find(ShoppingListInfo.class, id);
			entityManager.remove(shoppingListInfo);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<ShoppingListInfo> getBreadFromShoppingList() {
		try {
			Query theQuery = entityManager.createQuery("from ShoppingListInfo where catagory='bread'" , ShoppingListInfo.class);
			List<ShoppingListInfo> theList = theQuery.getResultList();
			return theList;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<ShoppingListInfo> getVegetablesFromShoppingList() {
		Query theQuery = entityManager.createQuery("from ShoppingListInfo where catagory='vegetables'", ShoppingListInfo.class);
		List<ShoppingListInfo> theList = theQuery.getResultList();
		return theList;
	}

	@Override
	public List<ShoppingListInfo> getFruitsFromShoppingList() {
		Query theQuery = entityManager.createQuery("from ShoppingListInfo where catagory='fruits'", ShoppingListInfo.class);
		List<ShoppingListInfo> theList = theQuery.getResultList();
		return theList;
	}

	@Override
	public List<ShoppingListInfo> getDairyFromShoppingList() {
		Query theQuery = entityManager.createQuery("from ShoppingListInfo where catagory='dairy'", ShoppingListInfo.class);
		List<ShoppingListInfo> theList = theQuery.getResultList();
		return theList;
	}
	
	

}
