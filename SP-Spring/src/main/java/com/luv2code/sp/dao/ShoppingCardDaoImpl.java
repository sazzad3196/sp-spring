package com.luv2code.sp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.sp.entity.CardInfo;
import com.luv2code.sp.entity.ShoppingListInfo;

@Repository
@Transactional
public class ShoppingCardDaoImpl implements ShoppingCardDao {
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public boolean saveOrUpdateShoppingCard(int userId, int itemId, int quantity) {
		try {
			ShoppingListInfo shoppingListInfo = entityManager.find(ShoppingListInfo.class, itemId);
			CardInfo theCardInfo = new CardInfo(userId, quantity, 1);
			shoppingListInfo.add(theCardInfo);
			entityManager.persist(theCardInfo);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<CardInfo> getCardInfoByUserId(int userId, int status) {
		Query theQuery = entityManager.createQuery("from CardInfo where status='" + status + "' and userId='" + userId + "'", CardInfo.class);
		List<CardInfo> theList = theQuery.getResultList();
		//System.out.println("CardInfo: " + theList);
		for(CardInfo theCardInfo: theList) {
			System.out.println("Item Name: " + theCardInfo.getShoppingListInfo().getName());
		}
		return theList;
	}

	@Override
	public boolean deleteShoppingCard(int id) {
		try {
			CardInfo cardInfo = entityManager.find(CardInfo.class, id);
			entityManager.remove(cardInfo);
			return true;
		}catch (Exception e) {
			 e.printStackTrace();
		}
		return false;
	}

	@Override
	public CardInfo getCardInfoByCardId(int cardId) {
		CardInfo theCardInfo = entityManager.find(CardInfo.class, cardId);
		return theCardInfo;
	}

	@Override
	@Transactional
	public boolean saveOrUpdateShoppingCard(CardInfo cardInfo) {
		try {
			entityManager.merge(cardInfo);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
