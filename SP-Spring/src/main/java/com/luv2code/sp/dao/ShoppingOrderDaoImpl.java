package com.luv2code.sp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.sp.entity.CardInfo;
import com.luv2code.sp.entity.OrderInfo;
import com.luv2code.sp.entity.UserInfo;

@Repository
@Transactional
public class ShoppingOrderDaoImpl implements ShoppingOrderDao {
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public boolean saveOrUpdateShoppingOrder(int cardId, int userId, String address, String date, String city) {
		try {
			UserInfo theUserInfo = entityManager.find(UserInfo.class, userId);
			CardInfo theCardInfo = entityManager.find(CardInfo.class, cardId);
			theCardInfo.setStatus(2);
			OrderInfo theOrderInfo = new OrderInfo(address, date, city);
			theCardInfo.add(theOrderInfo);
			theUserInfo.add(theOrderInfo);
			entityManager.persist(theOrderInfo);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<OrderInfo> findAllByUserIdShoppingOrder(int userId) {
		 Query theQuery = entityManager.createQuery("from OrderInfo where userId='" + userId + "'", OrderInfo.class);
		 List<OrderInfo> theOrderInfos = theQuery.getResultList();
		 List<OrderInfo> list = new ArrayList();
		 for(OrderInfo theOrderInfo: theOrderInfos) {
			 if(theOrderInfo.getCardInfo().getStatus() == 2) { 
				 list.add(theOrderInfo);
			 }
		 }
		 
		 return list;
	}

	@Override
	public List<OrderInfo> findAllShoppingOrder() {
		 Query theQuery = entityManager.createQuery("from OrderInfo", OrderInfo.class);
		 List<OrderInfo> theOrderInfos = theQuery.getResultList();
		 List<OrderInfo> list = new ArrayList();
		 for(OrderInfo theOrderInfo: theOrderInfos) {
			 if(theOrderInfo.getCardInfo().getStatus() == 2) { 
				 list.add(theOrderInfo);
			 }
		 }
		 
		 return list;
	}

	@Override
	public boolean deleteShoppingOrder(int orderId) {
		try {
		    OrderInfo theOrderInfo = entityManager.find(OrderInfo.class, orderId);
		    entityManager.remove(theOrderInfo);
		    return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public OrderInfo getOrderInfoByOrderId(int orderId) {
		OrderInfo theOrderInfo = entityManager.find(OrderInfo.class, orderId);
		return theOrderInfo;
	}

}
